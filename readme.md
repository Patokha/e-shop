# E-shop
E-shop is a app with cliend-server side for users managing their products, selling, buying their thing.

## Settings
Before starting - please configure your environment settings:

`DB_HOST` - host of using app
`DB_USER` - user name, that'll be connected to DB
`DB_PASS` - password for connecting to DB
`APP_PORT` - app's port
`DB_NAME` - name of database, that'll be created with all the tables
`NODE_ENV` - type of used environment,
`EMAIL_SENDER_API_KEY` - API key for sending emails ([send grid docs](https://sendgrid.com/docs/ui/account-and-settings/api-keys/))
`STRIPE_SECRET_API_KEY` - API key for payments ([stripe docs](https://stripe.com/docs/keys))
`JWT_TOKEN_SECRET` - secret for generating JWT authorization tokens
## Usage
Starting server:
```
npm i
npm start
```

##DB
Currently used Postgres DB with Sequelize as ORM.

##Frond-End
Here is EJS render pages using as a client side.

##Storage
All admin images are stored in `images` folder at local machine also as
invoices (`invoices`), created for users' orders.





