import express from 'express';
import utilites from './helpers/utilites.mjs';
import defineUser from './middleware/user.mjs';
import errorMiddleware from './middleware/error.mjs';
import initializeRoutes from './routes/index';
import startingServer from './starting/server.mjs';
const app = express();

utilites(app, express);

app.use(defineUser);
initializeRoutes(app);

app.use(errorMiddleware);

(async () => await startingServer(app))();


