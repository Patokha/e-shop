import jwt from 'jsonwebtoken';
import config from '../starting/config.mjs';
import getError from '../helpers/error.mjs';

const {JWT_TOKEN_SECRET} = config;

function isAuth(req, res, next) {
  const token = req.cookies['x-auth-token'];
  if (!token) return next(getError('Access denied. No token provided', 401));
  try {
    const decoded = jwt.verify(token, JWT_TOKEN_SECRET);
    req.user = decoded;
    next();
  } catch (err) {
    return next(getError('Access denied. Invalid token', 400));
  }
}

export {isAuth};
