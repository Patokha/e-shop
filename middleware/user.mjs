import jwt from 'jsonwebtoken';
import {User} from './../models/index.mjs';
import getError from './../helpers/error.mjs';
import config from '../starting/config.mjs';
const {JWT_TOKEN_SECRET} = config;

const defineUser = async (req, res, next) => {
  try {
    const token = req.cookies['x-auth-token'];
    if (!token) {
      res.locals.pageName = null;
      res.locals.isAuthenticated = false;
      return next();
    }

    const decoded = jwt.verify(token, JWT_TOKEN_SECRET);
    const user = await User.findOne({where: {email: decoded.email}});
    if (!user) {
      res.locals.pageName = null;
      res.locals.isAuthenticated = false;
      return next();
    }

    res.locals.pageName = user.email;
    res.locals.isAuthenticated = true;
    return next();
  } catch (err) {
    next(getError(err));
  }
};

export default defineUser;
