const errorMiddleware = (error, req, res, next) => {
  if (error) {
    return res.status(error.httpStatusCode).render('500', {
      pageTitle: 'Error!',
      path: '/',
      textMessage: error.message || 'Some error occured',
      errorCode: error.httpStatusCode || 500,
    });
  }
};

export default errorMiddleware;
