import {createRelations} from '../models/relations.mjs';
import {sequelize} from './database.mjs';
import config from './config.mjs';
const {APP_PORT: PORT} = config;

const startingServer = async (app) => {
  try {
    await createRelations();

    // await sequelize.sync({forse: true});
    await sequelize.authenticate();
    app.listen(PORT, () => console.log(`Server is running at ${PORT}`));
  } catch (error) {
    console.log(error);
  }
};

export default startingServer;
