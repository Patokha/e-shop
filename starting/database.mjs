import Sequelize from 'sequelize';
import config from './config.mjs';

const {DB_HOST, DB_USER, DB_PASS, DB_NAME} = config;

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
  host: DB_HOST,
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    idle: 10000,
  },
});

export {sequelize};
