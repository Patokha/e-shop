import dotenv from 'dotenv';
dotenv.config();

const {
  DB_HOST,
  DB_USER,
  DB_PASS,
  APP_PORT,
  DB_NAME,
  NODE_ENV,
  EMAIL_SENDER_API_KEY,
  STRIPE_SECRET_API_KEY,
  JWT_TOKEN_SECRET,
} = process.env;

export default {
  DB_HOST,
  DB_USER,
  DB_PASS,
  APP_PORT,
  DB_NAME,
  NODE_ENV,
  EMAIL_SENDER_API_KEY,
  STRIPE_SECRET_API_KEY,
  JWT_TOKEN_SECRET,
};
