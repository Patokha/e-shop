// eslint-disable-next-line no-unused-vars
const deleteProduct = (elem) => {
  const id = elem.parentNode.querySelector('[name=productId').value;
  const productElement = elem.closest('article');
  fetch(`/products/${id}`, {
    method: 'DELETE',
  }).then((result) => result.json())
    .then((() => {
      const parent = productElement.parentNode;
      productElement.remove();
      if (parent.children.length === 0) {
        document.getElementsByTagName('main')[0].innerHTML = '<h1>No products found....</h1>';
      }
    }))
    .catch((error) => console.log(error));
};
