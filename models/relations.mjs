import {Product, User, Cart, CartItem, Order, OrderItem} from './index';

const createRelations = () => {
  Product.belongsTo(User, {constraints: true, onDelete: 'CASCADE'});
  // // TOASK ??? how to not delete all when deleting product;
  User.hasMany(Product);
  User.hasOne(Cart);
  Cart.belongsTo(User);
  Cart.belongsToMany(Product, {through: CartItem}); // one cart can have many products, defining through what association it should be related
  Product.belongsToMany(Cart, {through: CartItem}); // one product can be in multiple carts

  Order.belongsTo(User);
  User.hasMany(Order);

  return Order.belongsToMany(Product, {constraints: false, onUpdate: 'SET NULL', onDelete: 'SET NULL', through: OrderItem});
};

export {createRelations};
