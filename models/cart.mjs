
import Sequelize from 'sequelize';
import {sequelize} from '../starting/database.mjs';

const Cart = sequelize.define('cart', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
});


export {Cart};
