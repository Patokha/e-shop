import Sequelize from 'sequelize';
import {sequelize} from '../starting/database.mjs';

const OrderItem = sequelize.define('orderItem', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  quantity: Sequelize.INTEGER,
});


export {OrderItem};
