import Sequelize from 'sequelize';
import {sequelize} from '../starting/database.mjs';

const CartItem = sequelize.define('cartItem', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  quantity: Sequelize.INTEGER,
});

export {CartItem};
