import Sequelize from 'sequelize';
import {sequelize} from '../starting/database.mjs';

const Order = sequelize.define('order', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
});

export {Order};
