import Sequelize from 'sequelize';
import {sequelize} from '../starting/database.mjs';

const Product = sequelize.define('product', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
    len: {
      args: [5, 30],
    },
  },
  description: {
    type: Sequelize.STRING,
    len: {
      args: [0, 250],
    },
  },
  imageUrl: {
    allowNull: false,
    type: Sequelize.STRING,
    len: {
      args: [20, 150],
    },
  },
  price: {
    allowNull: false,
    type: Sequelize.DOUBLE,
  },
});

export {Product};
