import methodOverride from 'method-override';
import path from 'path';
import cookieParser from 'cookie-parser';
import multer from 'multer';
import {storage, fileFilter} from './storage.mjs';

const utilites = (app, express) => {
  app.set('view engine', 'ejs');
  app.set('views', 'views');
  app.use(cookieParser());
  app.use(methodOverride('_method'));
  app.use(express.urlencoded({extended: true}));
  app.use(multer({storage, fileFilter}).single('image'));
  app.use(express.static(path.join(path.resolve(), 'public')));
  app.use('/images', express.static(path.join(path.resolve(), 'images')));
};

export default utilites;
