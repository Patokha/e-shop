const getError = (err, statusCode = 500) => {
  const error = new Error(err);
  error.httpStatusCode = statusCode;
  return error;
};

export default getError;
