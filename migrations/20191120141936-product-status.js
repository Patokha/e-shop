'use strict';

export default {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('Product', 'isInStock', {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      }),
      queryInterface.addColumn('Product', 'amountInStock', {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        min: 0,
      }),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        'Product',
        'isInStock'
      ),
      queryInterface.removeColumn(
        'Product',
        'amountInStock'
      ),
    ]);
  },
};
