'use strict';

export default {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('User', 'age', {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: false,
      defaultValue: 18,
      min: 1,
      max: 100,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'User',
      'age'
    );
  },
};
