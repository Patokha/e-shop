const getErrorPage = (req, res, next) => {
  res.status(404).render('error', {
    pageTitle: 'Page not found',
    path: '/error',
  });
};

const getServerErrorPage = (req, res, next) => {
  res.status(500).render('500', {
    pageTitle: 'Error!',
    path: '/500',
    textMessage: '',
  });
};

export {getErrorPage, getServerErrorPage};

