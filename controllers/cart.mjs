import {Product, User} from '../models/index.mjs';
import getError from '../helpers/error.mjs';

const getCart = async (req, res, next) => {
  try {
    const user = await User.findOne({where: {email: req.user.email}});
    const cart = await user.getCart();
    const products = await cart.getProducts();
    res.render('shop/cart', {
      pageTitle: 'Cart page',
      path: '/cart',
      products,
    });
  } catch (err) {
    next(getError(err));
  }
};
const postItemToCart = async (req, res, next) => {
  try {
    const {productId} = req.body;
    let newQuantity = 1;
    const user = await User.findOne({where: {email: req.user.email}});
    const cart = await user.getCart();
    const products = await cart.getProducts({where: {id: productId}});
    let product;
    if (products.length > 0) product = products[0];
    if (product) {
      const oldQuantity = product.cartItem.quantity;
      newQuantity = oldQuantity + 1;
    }
    const p = await Product.findByPk(productId);
    await cart.addProduct(p, {through: {quantity: newQuantity}});
    res.redirect('/cart');
  } catch (err) {
    next(getError(err));
  }
};

const deleteCartItem = async (req, res, next) => {
  try {
    const {id: prodId} = req.params;
    const user = await User.findOne({where: {email: req.user.email}});
    const cart = await user.getCart();
    const [product] = await cart.getProducts({where: {id: prodId}});
    await product.cartItem.destroy();
    res.redirect('/cart');
  } catch (err) {
    next(getError(err));
  }
};
export {
  getCart,
  postItemToCart,
  deleteCartItem,
};


