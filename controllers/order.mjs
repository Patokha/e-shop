import fs from 'fs';
import path from 'path';
import PDFDocument from 'pdfkit';
import stripeModule from 'stripe';

import config from '../starting/config.mjs';
import {Order, User} from '../models/index.mjs';
import getError from '../helpers/error.mjs';

const {STRIPE_SECRET_API_KEY} = config;
const stripe = stripeModule(STRIPE_SECRET_API_KEY);

const getOrders = async (req, res, next) => {
  try {
    const user = await User.findOne({where: {email: req.user.email}});
    const orders = await user.getOrders({include: ['products']});
    res.render('shop/orders', {
      pageTitle: 'Orders page',
      path: '/orders',
      orders,
    });
  } catch (err) {
    next(getError(err));
  }
};

const getOrderDetails = async (req, res, next) => {
  try {
    const {id: orderId} = req.params;
    const user = await User.findOne({where: {email: req.user.email}});
    const [order] = await user.getOrders({where: {id: orderId}, include: ['products']});
    res.render('shop/order-details', {
      pageTitle: `Order page #${order.id}`,
      path: '/orders',
      order,
    });
  } catch (err) {
    next(getError(err));
  }
};

const postOrder = async (req, res, next) => {
  try {
    console.log('here in order');
    const {stripeToken: token} = req.body;
    console.log('here in order2');
    const user = await User.findOne({where: {email: req.user.email}});
    const cart = await user.getCart();
    console.log('here in order 3');
    const products = await cart.getProducts();
    const order = await Order.create({userId: req.user._id});
    const totalSum = products.reduce((sum, {price, cartItem}) => (sum + price * cartItem.quantity), 0);
    const data = await order
      .addProducts(products.map((p) => {
        p.orderItem = {
          quantity: p.cartItem.quantity,
        };
        return p;
      }
      ));

    // eslint-disable-next-line
    const charge = stripe.charges.create({
      amount: totalSum * 100,
      currency: 'usd',
      source: token,
      description: 'Demo order by stripe',
      metadata: {order_id: data.id},
    });
    await cart.setProducts(null);
    res.redirect('/orders');
  } catch (err) {
    console.log('EEE orders', err);
    next(getError(err));
  }
};

const getInvoice = async (req, res, next) => {
  try {
    const {id} = req.params;
    const order = await Order.findByPk(id, {include: ['products']});
    if (!order) return next(new Error('No order found.'));

    if (order.userId !== req.user._id) {
      return next(new Error('Unauthorized'));
    }

    const invoiceFile = `invoice-${id}/${order.createdAt.toDateString()}.pdf`;
    const invoicePath = path.join('invoices', invoiceFile);
    res.setHeader('Content-Type', 'application/pdf');
    const pdfDoc = new PDFDocument();

    pdfDoc.pipe(fs.createWriteStream(invoicePath));
    pdfDoc.pipe(res);

    pdfDoc.fontSize(26).text('Invoice', {
      underline: true,
    });
    const totalSum = order.products.reduce((sum, i) => (sum + i.price * i.orderItem.quantity), 0);
    order.products.forEach(({title, price, orderItem}) => {
      pdfDoc.fontSize(14).text(`${title} - ${orderItem.quantity} x ${price}$`);
    });
    pdfDoc.text('________________');
    pdfDoc.fontSize(30).text(`Total price: ${totalSum}$`);

    pdfDoc.end();
  } catch (err) {
    next(getError(err));
  }

  // read stream from file
  // const file = fs.createReadStream(invoicePath);
  // res.setHeader('Content-Type', 'application/pdf');
  // res.setHeader('Content-Disposition', 'inline; filename="' + invoiceFile + '"');
  // file.pipe(res);
};

const getCheckout = async (req, res, next) => {
  try {
    const user = await User.findOne({where: {email: req.user.email}});
    const cart = await user.getCart();
    const products = await cart.getProducts();
    res.render('shop/checkout', {
      pageTitle: 'Checkout',
      path: '/checkout',
      products,
      totalSum: products.reduce((sum, {cartItem, price}) => sum += cartItem.quantity * price, 0),
    });
  } catch (err) {
    next(getError(err));
  }
};

export {
  getCheckout,
  getOrders,
  postOrder,
  getOrderDetails,
  getInvoice,
};
