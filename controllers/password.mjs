import bcript from 'bcryptjs';
import nodeMailer from 'nodemailer';
import sendGridTransport from 'nodemailer-sendgrid-transport';
import crypto from 'crypto';
import Sequelize from 'sequelize';

import {User} from '../models/index.mjs';
import config from '../starting/config.mjs';
import getError from '../helpers/error.mjs';

const {APP_PORT, EMAIL_SENDER_API_KEY} = config;
const Op = Sequelize.Op;

const tranporter = nodeMailer.createTransport(sendGridTransport({
  auth: {
    api_user: EMAIL_SENDER_API_KEY,
  },
}));

const renderLogin = (res, {errorMessage, validationErrors, email, password}) => {
  return res.status(422).render('auth/login', {
    path: '/login',
    pageTitle: 'Login page',
    errorMessage,
    validationErrors,
    oldData: {
      email,
      password,
    },
  });
};

const getReset = (req, res, next) => {
  return res.render('auth/reset', {
    path: '/reset',
    pageTitle: 'Reset passwords',
    errorMessage: '',
  });
};

const getUpdatingPassword = async (req, res, next) => {
  try {
    const {token} = req.params;
    const user = await User.findOne({where: {resetToken: token, resetTokenExpiration: {[Op.gt]: Date.now()}}});
    if (user) {
      return res.render('auth/update-password', {
        path: '/password',
        pageTitle: 'Update password',
        errorMessage: '',
        userEmail: user.email,
        resetToken: user.resetToken,
      });
    }

    return renderLogin(res, {
      errorMessage: 'No account found with such token or token has expired',
      validationErrors: [],
      email: null,
      password: null,
    });
  } catch (err) {
    next(getError(err));
  }
};

const postReset = async (req, res, next) => {
  try {
    const {email} = req.body;
    return crypto.randomBytes(32, async (err, buffer) => {
      if (err) {
        return res.render('auth/reset', {
          path: '/reset',
          pageTitle: 'Reset passwords',
          errorMessage: 'Error while creating an token for your resetting email!',
        });
      }

      const token = buffer.toString('hex');
      const user = await User.findOne({where: {email}});

      if (!user) {
        return res.render('auth/reset', {
          path: '/reset',
          pageTitle: 'Reset passwords',
          errorMessage: 'No account with that email found!',
        });
      }

      user.resetToken = token;
      user.resetTokenExpiration = Date.now() + 360000;
      const result = await user.save();

      if (result) {
        await tranporter.sendMail({
          to: email,
          from: 'e-shop@node-express.com',
          subject: 'Password reset',
          html: `<p>You requested a password reset</p>
            <p>Click the link <a href='http://localhost:${APP_PORT}/reset/${token}'>RESET</a> to set a new password.</p>
            <p>Ignore this email if this link was send mistakenly</p>`,
        });
        return res.redirect('/login');
      }
    });
  } catch (err) {
    next(getError(err));
  }
};

// update-password
const postNewPassword = async (req, res, next) => {
  try {
    const {password, confirmPassword, resetToken, email} = req.body;
    if (password !== confirmPassword) {
      return res.render('auth/reset', {
        path: `/reset/${resetToken}`,
        pageTitle: 'Reset passwords',
        errorMessage: 'Passwords not match',
      });
    }
    const user = await User.findOne({where: {email, resetToken, resetTokenExpiration: {[Op.gt]: Date.now()}}});
    if (!user) {
      return res.render('auth/reset', {
        path: `/reset/${resetToken}`,
        pageTitle: 'Reset passwords',
        errorMessage: 'No account found with such email or token has expired',
      });
    }
    const isPasswordsMatch = await bcript.compare(password, user.password);
    if (isPasswordsMatch) {
      return res.render('auth/reset', {
        path: `/reset/${resetToken}`,
        pageTitle: 'Reset passwords',
        errorMessage: 'Please enter password, you did not used before',
      });
    }

    const userPassword = await bcript.hash(password, 12);
    user.password = userPassword;
    user.resetToken = null;
    user.resetTokenExpiration = undefined;
    user.save();
    return res.redirect('/login');
  } catch (err) {
    next(getError(err));
  }
};

export {
  getReset,
  postReset,
  getUpdatingPassword,
  postNewPassword,
};
