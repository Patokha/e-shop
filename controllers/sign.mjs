import bcript from 'bcryptjs';
import nodeMailer from 'nodemailer';
import sendGridTransport from 'nodemailer-sendgrid-transport';
import validation from 'express-validator';

import {User} from '../models/index.mjs';
import config from '../starting/config.mjs';
import getError from '../helpers/error.mjs';

const {validationResult, check} = validation;

const {EMAIL_SENDER_API_KEY} = config;

const tranporter = nodeMailer.createTransport(sendGridTransport({
  auth: {
    api_user: EMAIL_SENDER_API_KEY,
  },
}));

const getSignupPage = (req, res, next) => {
  res.render('auth/signup', {
    pageTitle: 'Signup page',
    path: '/signup',
    oldData: {
      email: null,
      password: null,
    },
    validationErrors: [],
    errorMessage: '',
  });
};

const signUpValidation = () => {
  return [
    check('email')
      .isEmail()
      .withMessage('Please enter a valid email')
      .normalizeEmail()
      .custom((value, {req}) => {
        return User.findOne({where: {email: value}})
          .then((userData) => {
            if (userData) {
              // eslint-disable-next-line
              return Promise.reject('Email is already in use.');
            }
          });
      }),
    check('password', 'Please enter a password at least 5 charaters with only numbers and text')
      .isLength({min: 5})
      .trim()
      .isAlphanumeric(),

    check('confirmPassword', 'Please enter a password at least 5 charaters with only numbers and text')
      .isLength({min: 5})
      .trim()
      .isAlphanumeric()
      .custom((value, {req}) => {
        if (value !== req.body.password) {
          throw new Error('Passwords have to match!');
        }

        return true;
      }),
  ];
};

const postSignupPage = async (req, res, next) => {
  try {
    const {email, password} = req.body;
    const errors = await validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).render('auth/signup', {
        pageTitle: 'Signup page',
        path: '/signup',
        errorMessage: errors.array()[0].msg,
        validationErrors: errors.array(),
        oldData: {
          email,
          password,
        },
      });
    }

    const hashedPswd = await bcript.hash(password, 12);// hash in string to hash, 0...12
    const user = await User.create({email, password: hashedPswd});
    const cart = await user.getCart(({where: {userId: user.id}}));
    if (!cart) await user.createCart();
    // sending mail
    await tranporter.sendMail({
      to: email,
      from: 'e-shop@node-express.com',
      subject: 'Signup succeeded',
      html: '<h1>You successfully sign in at <b>E-SHOP</b></h1>',
    });
    return res.redirect('/login');
  } catch (err) {
    next(getError(err));
  }
};

export {
  getSignupPage,
  postSignupPage,

  signUpValidation,
};
