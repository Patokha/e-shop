import bcript from 'bcryptjs';
import jwt from 'jsonwebtoken';
import nodeMailer from 'nodemailer';
import validation from 'express-validator';
const {validationResult, check} = validation;
import sendGridTransport from 'nodemailer-sendgrid-transport';

import {User} from '../models/index.mjs';
import getError from '../helpers/error.mjs';
import config from '../starting/config.mjs';

const {JWT_TOKEN_SECRET, EMAIL_SENDER_API_KEY} = config;
const tranporter = nodeMailer.createTransport(sendGridTransport({
  auth: {
    api_user: EMAIL_SENDER_API_KEY,
  },
}));

const renderLogin = (res, {errorMessage, validationErrors, email, password}) => {
  return res.status(422).render('auth/login', {
    path: '/login',
    pageTitle: 'Login page',
    errorMessage,
    validationErrors,
    oldData: {
      email,
      password,
    },
  });
};

const getLoginPage = (req, res, next) => {
  res.render('auth/login', {
    path: '/login',
    pageTitle: 'Login page',
    validationErrors: [],
    errorMessage: '',
    oldData: {
      email: null,
      password: null,
    },
  });
};

const loginValidation = () => {
  return [
    check('email')
      .isEmail()
      .normalizeEmail()
      .withMessage('Please enter a valid email'),
    check('password', 'Please enter a password at least 5 charaters with only numbers and text')
      .isLength({min: 5})
      .trim()
      .isAlphanumeric(),
  ];
};

const postLogin = async (req, res, next) => {
  try {
    const {email, password} = req.body;
    const errors = await validationResult(req);
    // console.log('errors', errors);
    if (!errors.isEmpty()) {
      return renderLogin(res, {
        errorMessage: errors.array()[0].msg,
        validationErrors: errors.array(),
        email,
        password,
      });
    }

    const user = await User.findOne({where: {email}});
    // console.log('user', user);
    if (!user) {
      return renderLogin(res, {
        errorMessage: 'Invalid email or password',
        validationErrors: [],
        email,
        password,
      });
    }
    // hash out
    const isPasswordsMatch = await bcript.compare(password, user.password);
    // console.log('isPasswordsMatch', isPasswordsMatch);
    if (isPasswordsMatch) {
      const token = jwt.sign({_id: user.id, email: user.email}, JWT_TOKEN_SECRET, {expiresIn: '1h'});
      res.cookie('x-auth-token', token, {maxAge: `${1000 * 60 * 60}`, httpOnly: true});
      await tranporter.sendMail({
        to: user.email,
        from: 'e-shop@node-express.com',
        subject: 'System entering',
        html: `<h1>You successfully sign in at <b>E-SHOP</b></h1>
        <p>We have noticed that you enter your system'</p>
        <p>Enter time: ${new Date().toLocaleString()}</p>`,
      });
      return res.redirect('/');
    }

    return renderLogin(res, {
      errorMessage: 'Invalid email or password',
      validationErrors: [],
      email,
      password,
    });
  } catch (err) {
    next(getError(err));
  }
};

const postLogout = (req, res, next) => {
  res.cookie('x-auth-token', '', {maxAge: 0});
  return res.redirect('/');
};

export {
  postLogin,
  postLogout,
  getLoginPage,

  loginValidation,
};
