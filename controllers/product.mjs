import jwt from 'jsonwebtoken';
import validation from 'express-validator';
const {validationResult} = validation;

import deleteFile from '../helpers/file.mjs';
import getError from '../helpers/error.mjs';
import config from '../starting/config.mjs';
import {Product, User} from '../models/index.mjs';
const {JWT_TOKEN_SECRET} = config;

const caclulatePagination = async (req, {pageTitle, path}, itemsPerPage = 3) => {
  const page = +req.query.page || 1;
  let totalAmount;
  const requestParams = {
    limit: itemsPerPage,
    offset: ((page - 1) * itemsPerPage),
  };

  await Product.count().then((c) => totalAmount = +c);

  if (!page) {
    requestParams.offset = 0;
  }

  return {
    requestParams,
    renderData: {
      pageTitle,
      path,
      pagination: {
        currentPage: page,
        hasNextPage: (itemsPerPage * page) < totalAmount,
        hasPreviousPage: page > 1,
        nextPage: page + 1,
        previousPage: page - 1,
        lastPage: Math.ceil(totalAmount / itemsPerPage),
        counter: (page > 3) ? page - 2 : 1,
      },
    },
  };
};

const renderPage = async (req, res, {requestParams, renderPage, renderData}) => {
  const products = await Product.findAll(requestParams);
  const token = req.cookies['x-auth-token'];
  if (token) {
    const decoded = jwt.verify(token, JWT_TOKEN_SECRET);
    const user = await User.findOne({where: {email: decoded.email}});
    if (user) {
      return res.render(renderPage, {
        ...renderData,
        products,
        ownerId: user.id,
      });
    }
  }

  return res.render(renderPage, {
    ...renderData,
    products,
    ownerId: '',
  });
};

const getAddProduct = (req, res, next) => {
  res.render("admin/edit-product", {
    pageTitle: "Add product",
    path: "/admin/products/new",
    editing: false,
    hasError: false,
    product: {},
    validationErrors: [],
    errorMessage: '',
  });
};

const getEditProduct = async (req, res, next) => {
  const {id: productId} = req.params;

  try {
    const product = await Product.findByPk(productId);

    if (!product) return res.redirect('/');

    if (product.userId !== req.user._id) return res.redirect('/');

    res.render("admin/edit-product", {
      pageTitle: "Edit product",
      path: `/admin/products/${productId}/edit`,
      editing: true,
      product,
      errors: [],
      hasError: false,
      validationErrors: [],
      errorMessage: '',
    });
  } catch (err) {
    next(getError(err));
  }
};

const {check} = validation;

const addEditProductValidation = () => {
  return [
    check('title')
      .isString()
      .isLength({min: 5, max: 30})
      .trim()
      .withMessage('Title must be min 5 char - max - 30 char length'),
    check('description', 'Description should be max 250 char length')
      .isLength({min: 0, max: 250})
      .trim(),
    check('price', 'Price can`t be negative value and should be more than 0')
      .isFloat({min: 0}),
  ];
};

const postProduct = async (req, res, next) => {
  try {
    const {title, price, description} = req.body;
    const {file} = req;

    if (!file) {
      return res.status(422).render("admin/edit-product", {
        pageTitle: "Add product",
        path: "/admin/products/new",
        editing: false,
        hasError: true,
        product: {
          title,
          description,
          price: parseFloat(price),
        },
        errorMessage: 'Attached file is not an image',
        validationErrors: [],
      });
    }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).render("admin/edit-product", {
        pageTitle: "Add product",
        path: "/admin/products/new",
        editing: false,
        hasError: true,
        product: {
          title,
          description,
          price: parseFloat(price),
        },
        errorMessage: errors.array()[0].msg,
        validationErrors: errors.array(),
      });
    }

    const {path: imageUrl} = file;
    await Product.create({
      title,
      price,
      imageUrl,
      description,
      userId: req.user._id,
    });
    return res.redirect("/admin/products");
  } catch (err) {
    next(getError(err));
  }
};

const deleteAdminProduct = async (req, res, next) => {
  const {id: productId} = req.params;
  try {
    const product = await Product.findByPk(productId);
    if (product.userId !== req.user._id) return res.redirect('/');

    deleteFile(product.imageUrl);
    await product.destroy();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: 'Deleting fails'});
  }
};

const getAdminProducts = async (req, res, next) => {
  try {
    const user = await User.findOne({where: {email: req.user.email}});
    const products = await user.getProducts();
    res.render("admin/products", {
      pageTitle: "Admin products",
      path: "/admin/products",
      products,
    });
  } catch (err) {
    next(getError(err));
  }
};

const editProduct = async (req, res, next) => {
  try {
    const {title, price, description} = req.body;
    const {id} = req.params;
    const {file} = req;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).render("admin/edit-product", {
        pageTitle: "Edit product",
        path: `/admin/products/${id}/edit`,
        editing: true,
        hasError: true,
        validationErrors: errors.array(),
        errorMessage: errors.array()[0].msg,
        product: {
          title,
          description,
          price: parseFloat(price),
          id,
        },
      });
    }

    const product = await Product.findByPk(id);
    if (product.userId !== req.user._id) return res.redirect('/');

    product.price = price;
    product.description = description;
    product.title = title;
    if (file) {
      product.imageUrl = file.path;
    }

    await product.save();
    return res.redirect("/admin/products");
  } catch (err) {
    next(getError(err));
  }
};

const getProducts = async (req, res, next) => {
  try {
    const data = await caclulatePagination(req, {pageTitle: 'Products list', path: '/products'});
    const params = {
      ...data,
      renderPage: 'shop/product-list',
    };
    await renderPage(req, res, params);
  } catch (err) {
    next(getError(err));
  }
};

const getHomePage = async (req, res, next) => {
  try {
    const data = await caclulatePagination(req, {pageTitle: 'Home page', path: '/'});
    const params = {
      ...data,
      renderPage: 'shop/home',
    };
    await renderPage(req, res, params);
  } catch (err) {
    next(getError(err));
  }
};

const getProductDetails = async (req, res, next) => {
  try {
    const {id} = req.params;
    const product = await Product.findByPk(id);
    res.render('shop/product-details', {
      pageTitle: product.title,
      product,
      path: `/products/${id}/details`,
      isOwner: (req.user._id === product.userId) ? true : false,
    });
  } catch (err) {
    next(getError(err));
  }
};

export {
  getProducts,
  getHomePage,
  editProduct,
  postProduct,
  getAddProduct,
  getEditProduct,
  getAdminProducts,
  getProductDetails,
  deleteAdminProduct,

  addEditProductValidation,
};

