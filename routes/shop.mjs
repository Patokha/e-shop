import express from 'express';

// eslint-disable-next-line
const router = express.Router();
import {isAuth} from '../middleware/isAuth.mjs';

import {
  getProducts,
  getHomePage,
  getProductDetails,
} from "../controllers/product.mjs";

import {
  getCart,
  postItemToCart,
  deleteCartItem,
} from "../controllers/cart.mjs";
import {
  getOrders,
  getOrderDetails,
  getInvoice,
  getCheckout,
} from "../controllers/order.mjs";

router.get("/", getHomePage);

router.get("/products", getProducts);

router.get("/products/:id/details", isAuth, getProductDetails);

router.get("/cart", isAuth, getCart);

router.post("/cart", isAuth, postItemToCart);

router.delete("/cart/:id", isAuth, deleteCartItem);

router.get("/orders/:id", isAuth, getOrderDetails);

router.get("/orders", isAuth, getOrders);

router.get("/orders/:id/invoice", isAuth, getInvoice);

router.get("/checkout", isAuth, getCheckout);

export {router};

