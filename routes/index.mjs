import {router as adminRoutes} from './admin.mjs';
import {router as shopRoutes} from './shop.mjs';
import {router as errorRoute} from './error.mjs';
import {router as loginRoute} from './auth.mjs';

import {isAuth} from './../middleware/isAuth.mjs';
import {postOrder} from "./../controllers/order.mjs";

const initializeRoutes = (app) => {
  app.post('/orders', isAuth, postOrder);

  app.use(adminRoutes);
  app.use(shopRoutes);
  app.use(loginRoute);
  app.use(errorRoute);
};

export default initializeRoutes;
