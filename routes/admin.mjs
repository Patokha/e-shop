import express from 'express';

import {isAuth} from '../middleware/isAuth.mjs';

// eslint-disable-next-line
const router = express.Router();

import {
  getAddProduct,
  postProduct,
  getAdminProducts,
  getEditProduct,
  editProduct,
  deleteAdminProduct,

  addEditProductValidation,
} from '../controllers/product.mjs';

router.get('/admin/products/new', isAuth, getAddProduct);

router.get('/admin/products', isAuth, getAdminProducts);

router.get('/admin/products/:id/edit', isAuth, getEditProduct);

router.post('/products', isAuth, addEditProductValidation(), postProduct);

router.put('/products/:id', isAuth, addEditProductValidation(), editProduct);

router.delete('/products/:id', isAuth, deleteAdminProduct);

export {router};
