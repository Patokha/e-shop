import express from 'express';
// eslint-disable-next-line
const router = express.Router();

import {getServerErrorPage, getErrorPage} from '../controllers/error.mjs';

router.get('/500', getServerErrorPage);

router.use(getErrorPage);


export {router};
