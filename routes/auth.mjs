import express from 'express';

// eslint-disable-next-line
const router = express.Router();

import {
  getLoginPage,
  postLogin,
  postLogout,
  loginValidation,
} from '../controllers/login.mjs';
import {
  postSignupPage,
  getSignupPage,
  signUpValidation,
} from '../controllers/sign.mjs';
import {
  postReset,
  getReset,
  getUpdatingPassword,
  postNewPassword,
} from '../controllers/password.mjs';

router.get('/login', getLoginPage);

router.get('/signup', getSignupPage);

router.post('/signup', signUpValidation(), postSignupPage);

router.post('/login', loginValidation(), postLogin);

router.post('/logout', postLogout);

router.post('/reset', postReset);

router.get('/reset', getReset);

router.get('/reset/:token', getUpdatingPassword);

router.post('/password', postNewPassword);

export {router};
